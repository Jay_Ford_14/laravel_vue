<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="">

    <title>Laravel Vue JWT Auth</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <style>
        html{
            height : 100%
        }
    </style>
</head>
<body class="h-100">
    <div id="app" class="container-fluid d-flex h-100">
        <app-component></app-component>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>