import Vue from 'vue'
import VueToasted from 'vue-toasted'
import fontawesome from '@fortawesome/fontawesome'
import faSolid from '@fortawesome/fontawesome-free-solid'

Vue.use( VueToasted, {
    iconPack : 'fontawesome',
    duration : 2000,
    theme: "bubble", 
    position: "bottom-center", 
    singleton : true
});

Vue.toasted.register('showSuccessMsg',
    (payload) => {
        return `${fontawesome.icon(faSolid.faCheckCircle).html}` + payload.message;
    },
    {
        type : 'success'
    }
)


Vue.toasted.register('showErrorMsg',
    (payload) => {
        return `${fontawesome.icon(faSolid.faExclamationCircle).html}` + payload.message;
    },
    {
        type : 'error'
    }
)
