import Vue from 'vue';
import VueRouter from 'vue-router'
import store from './store'

//components 
import LoginComponent from './components/LoginComponent'
import RegisterComponent from './components/RegisterComponent'
import HomeComponent from './components/HomeComponent'

//home components
import MemoComponent from './components/home/MemoComponent'
import FavouriteMemosComponent from './components/home/FavouriteMemosComponent'

Vue.use(VueRouter)

const routes = [
    {
        path : "/",
        redirect : { name : 'login' }
    },
    {
        path : '/login',
        name : 'login',
        component : LoginComponent
    },
    {
        path : '/register',
        name : 'register',
        component : RegisterComponent
    },
    {
        path : '/home',
        component : HomeComponent,
        meta: { requiresAuth: true },
        children : [
            {
                path : '',
                name : 'home',
                component : MemoComponent
            },
            {
                path : 'favourites',
                name : 'favourites',
                component : FavouriteMemosComponent
            }
        ]

    }
]

const router = new VueRouter({
    mode : 'history',
    linkActiveClass: "active",
    routes
})

router.beforeEach((to, from, next) => {

    // check if the route requires authentication and user is not logged in
    if (to.matched.some(route => route.meta.requiresAuth) && !store.state.isLoggedIn) {
        // redirect to login page
        next({ name: 'login' })
        return
    }

    // if logged in redirect to dashboard
    if(to.path === '/login' && store.state.isLoggedIn) {
        next({ name: 'home' })
        return
    }

    next()
})

export default router