
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

import store from './store'
import router from './routes'
import toasted from './toasted'
import AppComponent from './components/AppComponent'

import moment from 'moment'
import VeeValidate from 'vee-validate'
import VuejsDialog from 'vuejs-dialog'

axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    const token = localStorage.getItem('token')
    if(token){
        config.headers.common['Authorization'] = 'Bearer ' + token
    }
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
  
    const originalRequest = error.config
    const originalToken = localStorage.getItem('token')
  
    if (error.response.status === 401 && !originalRequest._retry && originalToken) {
  
      originalRequest._retry = true;
  
      return axios.get('/api/auth/refresh')
        .then(({data}) => {
          let token = data.access_token;
          localStorage.setItem('token', token);
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
          originalRequest.headers['Authorization'] = 'Bearer ' + token;
          return axios(originalRequest);
        });

    }
  
    return Promise.reject(error);
});

Vue.filter('formatDate', function(value, format) {
    if (value) {
      return moment(String(value)).format(format)
    }
})

Vue.use( VeeValidate)
Vue.use( VuejsDialog)

const app = new Vue({
    components: { AppComponent },
    router,
    store
}).$mount('#app')

