<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::get('check/{email}', 'AuthController@check_email');
    Route::get('logout', 'AuthController@logout');
    Route::get('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');

});

Route::middleware('auth:api')->group(function () {
    Route::prefix('memo')->group(function()
    {
        Route::get('all', 'MemoController@getMemo');
        Route::post('create', 'MemoController@create');
        Route::get('show/{id}', 'MemoController@show');
        Route::post('update', 'MemoController@update');
        Route::delete('destroy/{id}', 'MemoController@destroy');

        Route::prefix('favourite')->group(function()
        {
            Route::get('all', 'MemoController@getFavMemo');
            Route::post('set', 'MemoController@setFavMemoStat');
        });
    });

});