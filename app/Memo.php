<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Memo extends Model
{
    //
    protected $fillable = [
        'user_id', 'title' ,'content', 'attached_file', 'favourite'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
