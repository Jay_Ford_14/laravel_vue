<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

use App\Memo;
use App\User;

use Storage;

class MemoController extends Controller
{
    public function getMemo()
    {
        return auth()->user()->memos()->orderBy('created_at', 'desc')->paginate(5);
    }

    public function create(Request $request)
    {
        $data = $request->except('attached_file');
        $data['attached_file'] = "";
        $file =  $request->file('attached_file');

        if($request->hasFile('attached_file'))
        {
            $data['attached_file'] = $this->getFileName($file);
        }

        try {
            Memo::create($data);
            if($file)
            {
                $path = $file->storeAs('public/images', $data['attached_file']);
                if(! $path){
                    return response()->json(['error' => 'Oops.. Something went wrong '], 500);
                }
            }
        } catch (QueryException $e){
            return response()->json(['error' => 'Oops.. Something went wrong '.$e->errorInfo], 500);
        }

        return response()->json(['success'=> 'Memo Added!'], 200);
    }

    public function show($id)
    {
        return Memo::find($id);
    }

    public function update(Request $request)
    {
        $data = $request->except('attached_file');
        $file =  $request->file('attached_file');

        $memo = Memo::find($data['id']);

        if($request->hasFile('attached_file'))
        {
            $data['attached_file'] = $this->getFileName($file);
            $old_image = $memo->attached_file;
        }

        try {
            $memo->update($data);
            if($file)
            {
                if($old_image)
                {
                    Storage::delete('/public/images/' . $old_image);
                }
                $path = $file->storeAs('public/images', $data['attached_file']);
                if(! $path)
                {
                    return response()->json(['error' => 'Oops.. Something went wrong '], 500);
                }
            }
        } catch (QueryException $e){
            return response()->json(['error' => 'Oops.. Something went wrong '.$e->errorInfo], 500);
        }

        return response()->json(['success'=> 'Memo Updated!'], 200);
    }

    protected function getFileName($file)
    {
        if(!$file)
        {
            return "";
        }
        
        $file_name_w_ext = $file->getClientOriginalName();
        $file_name = pathinfo($file_name_w_ext, PATHINFO_FILENAME);
        $ext = $file->getClientOriginalExtension();
        $file_name_to_store = $file_name . "_" . time() . "." . $ext;
        
        return $file_name_to_store;
    }

    public function destroy($id)
    {
        try {
            Memo::destroy($id);
        } catch (QueryException $e){
            return response()->json(['error' => 'Oops.. Something went wrong '.$e->errorInfo], 500);
        }

        return response()->json(['success'=> 'Memo Deleted!'], 200);
    }
    
    public function getFavMemo()
    {
        return auth()->user()->memos()->where('favourite', 1)->orderBy('created_at', 'desc')->paginate(5);
    }

    public function setFavMemoStat(Request $request)
    {
      
        try {
            $id = $request->id;
            $data = $request->except('id');
            Memo::find($id)->update($data);
        } catch (QueryException $e){
            return response()->json(['error' => 'Oops.. Something went wrong '.$e->errorInfo], 500);
        }

        return response()->json(['success'=> 'Memo '.($data['favourite'] === 1? 'Added' : 'Removed').' to Favourite!'], 200);
    }
}
